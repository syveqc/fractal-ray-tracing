from fractal_ray_tracing.raytracing import Scene, Camera, IFS, MaterialSolid, LightSource, Plane
import numpy as np
import json


scene = Scene()
scene.add_light_source(LightSource(np.array([3,0,1.8]), 1))

# create IFS
fs = np.array([[[0.5, 0,    0,    0],
                [0,    0.5,  0,    0],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],


                [[0.5,  0,    0,    0.5],
                [0,    0.5,  0,    0],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.5],
                [0,    0.5,  0,    0.5],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.0],
                [0,    0.5,  0,    0.5],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.25],
                [0,    0.5,  0,    0.25],
                [0,    0,    0.5,  0.5],
                [0,    0,    0,    1]]], np.float32)

ifs = IFS(fs, np.array([0,0,0]), MaterialSolid(np.array([1., 1.0, 0.0])), max_depth=10)
# add ifs
scene.add_object(ifs)
# add ground
scene.add_object(Plane(np.array([0,0,0]), np.array([0,0,1]), MaterialSolid(color=np.array([1.0, 1., 0.0]), reflection_weight=0.3)))

# create camera
camera = Camera(np.float32([7. ,5. ,1.5]), np.float32([0.5,0.7,0.1]), np.float32([0,0,1]), (1920, 1080), np.pi/6.5, -1, 0.004)

# save scene
json_string = scene.get_json_string(camera=camera, max_bounces=2, samples_per_ray=1)
with open("fractal_ray_tracing/scenes/sierpinski_gasket.scene", "w") as outfile:
    outfile.write(json_string.replace("\\n", "\n"))