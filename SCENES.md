# The `*.scene` file format

The `*.scene` file format (not to be confused with existing file formats) is a JSON format for defining a collection of renderable objects and lightsources and a camera which can render those objects. You can use as many line breaks as you want, since they get removed prior to reading the JSON in code.

`numpy` `ndarray`s are read using the `eval` function, so it is also possible to write expressions like `[1/3, 1-1/3, 0.]`.

## Structure

 - `background_color`: optional, 3d `ndarray` with values between 0 and 1, default is `[31./255., 104./255., 204./255.]`
 - `objects`: list of objects, defined by:
   - `type`: either `ifs`, `sphere` or `plane`. The rest of the attributes depend on the type, they are:
     - for `ifs`:
        - `fs`: $N\times 4\times 4$ `ndarray`, the contraction mappings making up the IFS
        - `origin`: 3d `ndarray`, the origin of the IFS, will not be its center, but rather a translation applied to the center (e.g. the sierpinski gasket has center around `[0.5, 0.5, 0.5]`, so origin `[1, -1, 0]` would translate to the object being centered at around `[1.5, -0.5, 0.5]`)
        - `transformation_matrix`: $3\times 3$ `ndarray` transformation matrix to be applied, e.g. rotation or scaling
        - `max_depth`: maximal recursion depth for intersection calculation of IFS, determines detail
        - `normal_calculation_depth`: over how many recursion levels the normals should be averaged, see antialiasing section
        - `material`: material to be used:
          - `type`: type of the material, the rest of the attributes depend on the type, but right now only `solid` is supported
          - `color`: 3d `ndarray` between 0 and 1 determining the color of the material
          - `k_ambient`: k_ambient parameter from the phong model
          - `k_diffus`: k_diffus parameter from the phong model
          - `k_spectacular`: k_spectacular parameter from the phong model
          - `n`: n parameter from the phong model
          - `reflection_weight`: between 0 and 1, how much of the color of the objects comes from the secondary ray color.
      - for `sphere`:
        - `origin`: 3d `ndarray` representing the origin of the sphere
        - `radius`: radius of the sphere
        - `material`: material to be used, see `ifs`
      - for `plane`:
        - `origin`: 3d `ndarray` representing a point the plane goes through
        - `normal_vector`: 3d `ndarray` representing the normal vector of the plane
        - `material`: material to be used, see `ifs`
  - `light_sources`: list of light sources, defined by:
    - `origin`: 3d `ndarray` representing the origin of the light source
    - `I_in`: light intensity parameter from the phong model
  - `camera`: camera to be used for rendering, optional for the gui, but if not given default camera will be used
    - `width`: width of the image to be rendered,
    - `height`: height of the image to be rendered,
    - `lookfrom`: 3d `ndarray` representing the origin of the camera, i.e. where it is positioned
    - `lookat`: 3d `ndarray` representing the point the camera looks at
    - `vup`: 3d `ndarray` determining where up is for the camera
    - `fov`: field of view in degrees
    - `focus_dist`: either:
      - a number determining the focus distance
      - -1, thend lookat will be in focus
      - a 3d `ndarray` representing a point that should be in focus
    - `aperture`: the aperture of the camera, determining depth of field
  - `max_bounces`: how often any ray may bounce on any surface
  - `samples_per_ray`: how many rays are used per pixel, useful for depth of field effects or (not supported yet) stochastic reflections in materials.

## Antialiasing
The effects of the `normal_calculation_depth` can be seen in this picture:
![Effects of antialiasing](fractal_ray_tracing/pics/antialiasing.png)
with `normal_calculation_depth` increasing from left to right. (`max_depth=15`, `normal_calculation_depth=5, 9, 13` from left to right.)