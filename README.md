# Fractal Ray Tracing

![Raytraced Fractal with Reflections](fractal_ray_tracing/pics/double_mirror.png)

The following is an implementation of the ["Efficient antialiased rendering of 3-D linear fractals"](https://dl.acm.org/doi/abs/10.1145/122718.122728) paper from 1991 by John C. Hart and Thomas A. DeFanti. 

## Installation
This is a pip installable package, so after cloning with
```
git clone https://gitlab.com/syveqc/fractal-ray-tracing
```
you can change into the top-level directory and run
```
pip install .
```
which should install all dependencies and the package. Testing was done with `python 3.10.9` and `3.9.13` on a computer with an AMD Ryzen 5950X, 64GB RAM and a Nvidia RTX 3090. Testing of the CPU mode was done in a VM, so the GPU would not be passed through. 

## Usage
To use the GUI, run
```
python run_fractal_raytracer_gui.py
```
You can then press the button "Load Scene" in the top-right, where you can load a `*.scene` file. (Several are provided in `fractal_ray_tracing/scenes`) The scene file format is described in [SCENES.md](SCENES.md).

You can then adjust the camera parameters (they work exactly as the corresponding JSON parameters, so see [SCENES.md](SCENES.md) for a description.) and the render parameters. `Samples per Ray` and `Maximal Bounces` work exactly as in the scene format as well. You can also toggle GPU calculation, which will usually be faster but may not be available on your computer. 32bit calculation will also be faster, but especially for big recursion depths fractals might be error-prone. (You can e.g. run `sierpinski_gasket.scene` in 32bit mode and see the gasket disappear almost completely, while it still is present in the reflection since the origin of the secondary rays is closer and thus the calculation works longer. For `menger_sponge.scene` setting it to 32bit precision makes it run forever even for small resolutions, which I have not yet fully debugged, but hypothesize to be due to intersection calculation breaking, meaning the algorithm has to go to the maximal recursion depth a lot more. See `sierpinski_gasket_32.png` and `menger_sponge_32.png`.)

After pressing `Go!` (which will only become clickable after loading a scene), a progress window will appear, telling you how far along the calculation is. The calculation is also non-cancellable, so if you decide you do not want to continue rendering, you have to kill the python process!

After an image is rendered, you may save it by clicking `Save Image`. Images of all provided scenes are located in `fractal_ray_tracing/pics`.

## Scene Creation
While simple scenes and IFSs can be created by hand by writing the JSON dictionary directly into a `*.scene` file, this can be very cumbersome for complex IFSs. See e.g. `generate_sierpinski_gasket_scene.py` for an example of how to generate scenes, and e.g. `generate_koch_scene.py` for an example of how to generate a more complex IFS.

## Usage within Python
All generation scripts serve as examples of how to use the library within python. You can also generate the given scenes by using the 
```python render_scene.py scene_file.scene``` 
which is provided in `fractal_ray_tracing/scenes`, or render all provided scenes by running the `fractal_ray_tracing/scenes/render_all_scenes.py` script. (Note that both scripts require a GPU, as the default is to render on GPU! They can be manually adapted by changing `on_gpu` to `False` in the `render_scene.py` script.) But beware that rendering `sierpinski_big_sphere_mirror.scene` takes a very long time (about 40min on my PC) and needs about 50GB of RAM, so it might be a good idea to remove this one if not enough resources are available!

As an example of how to render a complete scene using python only (so without `*.scene` files), see `render_mirror_scene.py`. (but beware, it takes a long time!)

## Known Bugs and Limitations
The rendering part of the progress is very slow, which is due to it being optimized poorly, running calculations in python instead of `numpy` optimized. This is due to the object-oriented approach of materials, etc. The bottleneck was only obvious when trying to create high-res images, which was very late in the project's runtime, making it very hard to revert the object-oriented approach there. Vectorizing the parameters and either using `numpy` arithmetic or sending the rendering to the GPU should greatly improve the performance of this step.
Also, RAM and VRAM might be very limiting, e.g. for `sierpinski_big_sphere_mirror.scene` my PC used about 50GB of RAM. 

## Authors and acknowledgment
The main author was Tobias Kietreiber. Some parts of `camera.py` and sphere intersections were taken from the [python-ray-tracing-with-cuda-example git repo](https://www.github.com/kwea123/python-ray-tracing-with-cuda-example). The paper for IFS ray tracing is [this one](https://dl.acm.org/doi/abs/10.1145/122718.122728) by John C. Hart and Thomas A. DeFanti as mentioned in the introduction. The Phong lighting model is from the [german Wikipedia article](https://de.wikipedia.org/wiki/Phong-Beleuchtungsmodell).

