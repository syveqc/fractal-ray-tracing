from fractal_ray_tracing.raytracing import Scene, Camera, IFS, MaterialSolid, LightSource, Plane
import numpy as np
import json

scene = Scene()
scene.add_light_source(LightSource(np.array([3,0,1.8]), 1))

# create IFS
angle = 2*np.pi/3
fs = []
rot_x = np.array([[1, 0, 0],
              [0, np.cos(angle), -np.sin(angle)],
              [0, np.sin(angle), np.cos(angle)]], np.float64)
rot_y = np.array([[np.cos(angle), 0, np.sin(angle)],
              [0, 1, 0],
              [-np.sin(angle), 0, np.cos(angle)]], np.float64)
rot_z = np.array([[np.cos(angle), -np.sin(angle), 0],
              [np.sin(angle), np.cos(angle), 0],
              [0, 0, 1]], np.float64)

base_f = np.array([[0.5, 0, 0],
              [0, 0.5, 0],
              [0, 0, 0.5]], np.float64)

f = np.copy(base_f)
vector = np.array([[0], [0], [1]], np.float64)
mat = np.zeros((4,4), np.float64)
mat[0:3, 0:3] = f
mat[0:3, 3:4] = 0.5*vector
mat[3,3] = 1
fs.append(np.expand_dims(mat, 0))
for rot in [rot_y, rot_z, rot_z]:
    f = np.matmul(rot, f)
    translate_vector = np.matmul(f, vector)
    mat = np.zeros((4,4), np.float64)
    mat[0:3, 0:3] = f
    mat[0:3, 3:4] = translate_vector
    mat[3,3] = 1
    fs.append(np.expand_dims(mat, 0))

angle = np.pi

rot_y_180 = np.array([[np.cos(angle), 0, np.sin(angle)],
              [0, 1, 0],
              [-np.sin(angle), 0, np.cos(angle)]], np.float64)

f = np.matmul(rot_y_180, base_f)
translate_vector = np.matmul(f, vector)
mat = np.zeros((4,4), np.float64)
mat[0:3, 0:3] = f
mat[0:3, 3:4] = translate_vector
mat[3,3] = 1
fs.append(np.expand_dims(mat, 0))

for rot in [rot_y, rot_z, rot_z]:
    f = np.matmul(rot, f)
    translate_vector = np.matmul(f, vector)
    mat = np.zeros((4,4), np.float64)
    mat[0:3, 0:3] = f
    mat[0:3, 3:4] = translate_vector
    mat[3,3] = 1
    fs.append(np.expand_dims(mat, 0))

fs = np.concatenate(fs, axis=0)

# add ifs
ifs = IFS(fs, np.array([0,0,0]), MaterialSolid(np.array([1., 1.0, 1.0])), max_depth=12)
scene.add_object(ifs)

# create camera
camera = Camera(np.float32([4.,2.,1.4]), np.float32([0.,0.,0.1]), np.float32([0,0,1]), (1920, 1080), np.pi/4, -1, 0.004)

# save scene
json_string = scene.get_json_string(camera=camera, max_bounces=1, samples_per_ray=1)
with open("fractal_ray_tracing/scenes/koch.scene", "w") as outfile:
    outfile.write(json_string.replace("\\n", "\n"))