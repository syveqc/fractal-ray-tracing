from setuptools import setup

setup(
    name='Factal Ray Tracing',
    version="1.0",
    description='Implementation of ray tracing for fractals, for more information see https://gitlab.com/syveqc/fractal-ray-tracing#fractal-ray-tracing',
    author='Tobias Kietreiber',
    install_requires=['numba',
                      'numpy',
                      'Pillow',
                      'PyQt6'
                      ] 
)
