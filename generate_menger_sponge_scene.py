from fractal_ray_tracing.raytracing import Scene, Camera, IFS, MaterialSolid, LightSource, Plane
import numpy as np
import json


scene = Scene()
scene.add_light_source(LightSource(np.array([3,-3,1.5]), 0.8))

# create IFS
fs = []
for i in [0,1,2]:
    for j in [0,1,2]:
        for k in [0,1,2]:
            if (i,j,k) == (1,1,0) or (i,j,k) == (1,1,2) or (i,j,k) == (1,1,1) or \
               (i,j,k) == (1,0,1) or (i,j,k) == (1,2,1) or (i,j,k) == (0,1,1) or (i,j,k) == (2,1,1):
                continue
            fs.append(np.array([[[1/3, 0,    0,    i/3],
                                [0,    1/3,  0,    j/3],
                                [0,    0,    1/3,  k/3],
                                [0,    0,    0,    1]]], np.float64))

fs = np.concatenate(fs, axis=0)
ifs = IFS(fs, np.array([0,0,0]), MaterialSolid(np.array([1.0, 0.5, 0.0])), max_depth=10, normal_calculation_depth=7)

# add ifs
scene.add_object(ifs)
# add ground
scene.add_object(Plane(np.array([0,0,0]), np.array([0,0,1]), MaterialSolid(color=np.array([1.0, 0.5, 0.0]), reflection_weight=0.3)))

# translate origin
origin, radius = ifs._get_bounding_sphere()
ifs.origin = np.array([0., 0., 0.5]) - origin

# create camera
camera = Camera(np.float32([7,2,2]), np.float32([0,0,0.3]), np.float32([0,0,1]), (1920, 1080), np.pi/6, -1, 0.004)

# save scene
json_string = scene.get_json_string(camera=camera, max_bounces = 2, samples_per_ray=1)
with open("fractal_ray_tracing/scenes/menger_sponge.scene", "w") as outfile:
    outfile.write(json_string.replace("\\n", "\n"))