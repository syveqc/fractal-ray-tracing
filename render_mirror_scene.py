from fractal_ray_tracing.raytracing import Scene, Camera, Sphere, IFS, MaterialSolid, LightSource, Plane
import numpy as np
from PIL import Image

scene = Scene()
scene.add_light_source(LightSource(np.array([3,0,1.5]), 1))

# construct ifs
fs = np.array([[[0.5, 0,    0,    0],
                [0,    0.5,  0,    0],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],


                [[0.5,  0,    0,    0.5],
                [0,    0.5,  0,    0],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.5],
                [0,    0.5,  0,    0.5],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.0],
                [0,    0.5,  0,    0.5],
                [0,    0,    0.5,  0],
                [0,    0,    0,    1]],

                [[0.5,  0,    0,    0.25],
                [0,    0.5,  0,    0.25],
                [0,    0,    0.5,  0.5],
                [0,    0,    0,    1]]], np.float32)

ifs = IFS(fs, np.array([0.5,0.5,0]), MaterialSolid(np.array([0.5, 1.0, 0.0])), max_depth=8)

# add "ground"
scene.add_object(Plane(np.array([0,0,0], np.float32), np.array([0,0,1.]), MaterialSolid(np.array([0.5, 1.0, 0.0]), reflection_weight=0.4)))
# add IFS
scene.add_object(ifs)
# add mirror sphere
scene.add_object(Sphere(np.array([0.5,-0.5,0.5]), 0.5, MaterialSolid(np.array([0.5, 1.0, 0.0]), reflection_weight=0.8)))

# add mirrors
scene.add_object(Plane(np.array([0.5,-2,0], np.float32), np.array([0,1.,-0.015]), MaterialSolid(np.array([0.5, 1.0, 0.0]), reflection_weight=0.99)))
scene.add_object(Plane(np.array([0.5,2,0], np.float32), np.array([0,1.,0.015]), MaterialSolid(np.array([0.5, 1.0, 0.0]), reflection_weight=0.99)))

# create camera
camera = Camera(np.float32([1.7,-1,0.3]), ifs._get_bounding_sphere()[0]+np.array([-0.3,0,-0.26]), np.float32([0,0,1]), (1920, 1080), np.pi/1.48, -1, 0.0002)

# render scene
max_bounces = 50 # turn down if you want faster render, 5 looks good as well
image = camera.render(scene, 1, max_bounces, precision=np.float64)

# save image
image = (255*image).astype(np.uint8)
Image.fromarray(image).save("fractal_ray_tracing/pics/double_mirror.png")