from PyQt6.QtWidgets import QApplication, QWidget, QMainWindow, QLabel, QHBoxLayout, QVBoxLayout, QPushButton, QLineEdit, QGroupBox, QGridLayout, QCheckBox, QFileDialog, QMessageBox
from fractal_ray_tracing.raytracing import Scene, Camera, Sphere, IFS, MaterialSolid, LightSource, Plane
from PyQt6.QtGui import QImage, QPixmap, QIntValidator, QDoubleValidator
from PyQt6.QtCore import Qt, QThread, pyqtSignal
import numpy as np
import json
from PIL import Image

from .progress_window import ProgressWindow

class FractalRayTracerApplication:
    def __init__(self):
        self.app = QApplication([])
    
        self.main_window = QMainWindow()
        self.main_window.setWindowTitle('Fractal Ray Tracer')
        self.main_window.setMinimumSize(1400, 720)
        self.calculation_thread = None
        
        central_widget = QWidget()
        main_layout = QHBoxLayout()
        left_layout = QHBoxLayout()
        right_layout = QVBoxLayout()
        self.image_label = QLabel()
        self.image = None
        self.scene = None
        left_layout.addWidget(self.image_label, 1)

        right_layout.addWidget(self.create_scene_box())
        right_layout.addWidget(self.create_camera_group_box())
        right_layout.addWidget(self.create_render_params_box())
        self.go_button = QPushButton("Go!")
        self.go_button.clicked.connect(self.run_calculation)
        self.go_button.setEnabled(False)
        self.save_button = QPushButton("Save Image")
        self.save_button.clicked.connect(self.save_image)
        self.save_button.setEnabled(False)

        right_layout.addWidget(self.go_button, 0)
        right_layout.addWidget(self.save_button, 0)
        main_layout.addLayout(left_layout)
        main_layout.addLayout(right_layout)
        central_widget.setLayout(main_layout)
        self.main_window.setCentralWidget(central_widget)

        camera = Camera(np.float32([7,0,0.7]), np.float32([0,0,0.1]), np.float32([0,0,1]), (1920, 1080), np.pi/5.5, -1, 0.0002)
        self.set_camera(camera)

    def show(self):
        self.main_window.show()
        self.app.exec()

    def set_image(self, image):
        self.image = image
        self.save_button.setEnabled(True)
        convert = QImage(image, image.shape[1], image.shape[0], image.strides[0], QImage.Format.Format_RGB888)
        pixmap = QPixmap.fromImage(convert)
        self.image_label.setPixmap(pixmap.scaled(self.image_label.width(), self.image_label.height(), Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.SmoothTransformation))

    def run_calculation(self):
        try:
            camera = self.get_camera()
        except Exception as ex:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Critical)
            msg.setText("Error while parsing camera!")
            msg.setInformativeText(str(ex))
            msg.setWindowTitle("Error")
            msg.exec()
            return

        samples_per_ray = int(self.samples_per_ray_edit.text())
        max_bounces = int(self.max_bounces_edit.text())

        self.calculation_thread = CalculationThread(self.scene, camera, samples_per_ray, max_bounces, self.float64_calculation.isChecked(), self.on_gpu.isChecked())
        self.progress_window = ProgressWindow(self.calculation_thread.progress_signal, len(self.scene.objects), max_bounces, len(self.scene.light_sources)*camera.W*camera.H*samples_per_ray)
        self.calculation_thread.progress_signal.connect(self.handle_image_signal)
        self.progress_window.show()
        self.calculation_thread.start()
        self.go_button.setEnabled(False)

    def save_image(self):
        dialog = QFileDialog(self.main_window)
        dialog.setDirectory(".")
        dialog.setFileMode(QFileDialog.FileMode.AnyFile)
        dialog.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        dialog.selectFile("unnamed.jpg")
        dialog.setNameFilter("Image files (*.png *.jpg *.jpeg *.gif)")
        dialog.setViewMode(QFileDialog.ViewMode.List)
        if dialog.exec():
            filenames = dialog.selectedFiles()
            if filenames:
                im = Image.fromarray(self.image)
                im.save(filenames[0])

    def handle_image_signal(self, progress_type, progress, msg):
        if progress_type == 5:
            self.set_image(self.calculation_thread.image)
            self.go_button.setEnabled(True)
            self.calculation_thread = None
            self.progress_window.close()
            self.progress_window = None
        if progress_type == 6:
            msg_box = QMessageBox()
            msg_box.setIcon(QMessageBox.Icon.Critical)
            msg_box.setText("Error while rendering!")
            msg_box.setInformativeText(msg)
            msg_box.setWindowTitle("Error")
            msg_box.exec()

            self.go_button.setEnabled(True)
            self.calculation_thread = None
            self.progress_window.close()
            self.progress_window = None

    def create_camera_group_box(self):
        cameraGroupBox = QGroupBox("Camera")

        self.width_edit = QLineEdit("1920")
        self.width_edit.setValidator(QIntValidator())
        self.height_edit = QLineEdit("1080")
        self.height_edit.setValidator(QIntValidator())
        self.lookfrom_edit = QLineEdit("5 0 0")
        self.lookat_edit = QLineEdit("0 0 0")
        self.vup_edit = QLineEdit("0 0 1")
        self.fov_edit = QLineEdit("120")
        self.fov_edit.setValidator(QIntValidator())
        self.focus_dist_edit = QLineEdit("-1")
        self.fov_edit.setValidator(QDoubleValidator())
        self.aperture_edit = QLineEdit("0.0001")
        self.aperture_edit.setValidator(QDoubleValidator())


        layout = QGridLayout()
        layout.addWidget(QLabel("Width: "), 0, 0)
        layout.addWidget(self.width_edit, 0, 1, 1, 2)
        layout.addWidget(QLabel("Height: "), 1, 0)
        layout.addWidget(self.height_edit, 1, 1, 1, 2)
        layout.addWidget(QLabel("Origin: "), 2, 0)
        layout.addWidget(self.lookfrom_edit, 2, 1, 1, 2)
        layout.addWidget(QLabel("Look at: "), 3, 0)
        layout.addWidget(self.lookat_edit, 3, 1, 1, 2)
        layout.addWidget(QLabel("Direction up: "), 4, 0)
        layout.addWidget(self.vup_edit, 4, 1, 1, 2)
        layout.addWidget(QLabel("Focal Distance: "), 5, 0)
        layout.addWidget(self.focus_dist_edit, 5, 1, 1, 2)
        layout.addWidget(QLabel("Field of View: "), 6, 0)
        layout.addWidget(self.fov_edit, 6, 1, 1, 2)
        layout.addWidget(QLabel("Aperture: "), 7, 0)
        layout.addWidget(self.aperture_edit, 7, 1, 1, 2)
        layout.setRowStretch(5, 1)
        cameraGroupBox.setLayout(layout)

        return cameraGroupBox


    def create_scene_box(self):
        scene_box = QGroupBox("Scene")

        self.objects_in_scene_label = QLabel("No Scene loaded yet!")
        self.lights_in_scene_label = QLabel("")
        open_scene_button = QPushButton("Load Scene")
        open_scene_button.clicked.connect(self.open_dialog)

        layout = QGridLayout()
        layout.addWidget(self.objects_in_scene_label, 0, 0)
        layout.addWidget(self.lights_in_scene_label, 1, 0,)
        layout.addWidget(open_scene_button, 2, 0, 1, 2)
        layout.setRowStretch(5, 1)
        scene_box.setLayout(layout)

        return scene_box

    def open_dialog(self):
        dialog = QFileDialog(self.main_window)
        dialog.setDirectory(".")
        dialog.setFileMode(QFileDialog.FileMode.ExistingFiles)
        dialog.setNameFilter("Scene files (*.scene)")
        dialog.setViewMode(QFileDialog.ViewMode.List)
        if dialog.exec():
            filenames = dialog.selectedFiles()
            if filenames:
                f = open(filenames[0], "r")
                print(filenames[0])
                try:
                    json_dict = json.loads(''.join(f.readlines()).replace("\n", ""))
                    self.scene = Scene.from_json(json_dict)
                    if "camera" in json_dict:
                        self.set_camera(Camera.from_json(json_dict["camera"]))
                    if "max_bounces" in json_dict:
                        self.max_bounces_edit.setText(str(json_dict["max_bounces"]))
                    if "samples_per_ray" in json_dict:
                        self.samples_per_ray_edit.setText(str(json_dict["samples_per_ray"]))
                    self.objects_in_scene_label.setText(f"{len(self.scene.objects) if self.scene is not None else 0} object(s) in scene")
                    self.lights_in_scene_label.setText(f"{len(self.scene.light_sources) if self.scene is not None else 0} light(s) in scene")
                    self.go_button.setEnabled(True)
                except Exception as ex:
                    msg = QMessageBox()
                    msg.setIcon(QMessageBox.Icon.Critical)
                    msg.setText("Error while reading scene/camera!")
                    msg.setInformativeText(str(ex))
                    msg.setWindowTitle("Error")
                    msg.exec()


    def create_render_params_box(self):
        cameraGroupBox = QGroupBox("Render Parameters")

        self.samples_per_ray_edit = QLineEdit("1")
        self.samples_per_ray_edit.setValidator(QIntValidator())
        self.max_bounces_edit = QLineEdit("5")
        self.max_bounces_edit.setValidator(QIntValidator())

        self.on_gpu = QCheckBox("Use GPU")
        self.on_gpu.setChecked(True)
        self.float64_calculation = QCheckBox("Use 64bit calculation")
        self.float64_calculation.setChecked(True)

        layout = QGridLayout()
        layout.addWidget(QLabel("Samples per Ray: "), 0, 0)
        layout.addWidget(self.samples_per_ray_edit, 0, 1, 1, 2)
        layout.addWidget(QLabel("Maximal Bounces: "), 1, 0)
        layout.addWidget(self.max_bounces_edit, 1, 1, 1, 2)
        layout.addWidget(self.on_gpu, 2, 1, 1, 2)
        layout.addWidget(self.float64_calculation, 3, 1, 1, 2)
        layout.setRowStretch(5, 1)
        cameraGroupBox.setLayout(layout)

        return cameraGroupBox

    def set_camera(self, camera:Camera):
        self.width_edit.setText(f"{camera.W}")
        self.height_edit.setText(f"{camera.H}")
        self.lookfrom_edit.setText(f"{np.array2string(camera.origin,  separator=',', suppress_small=True)}")
        self.lookat_edit.setText(f"{np.array2string(camera.lookat,  separator=',', suppress_small=True)}")
        self.vup_edit.setText(f"{np.array2string(camera.vup,  separator=',', suppress_small=True)}")
        self.fov_edit.setText(f"{np.degrees(camera.fov).astype(int)}")
        if type(camera.focus_dist_given) == np.ndarray:
            self.focus_dist_edit.setText(f"{np.array2string(camera.focus_dist_given,  separator=',', suppress_small=True)}")
        else:
            self.focus_dist_edit.setText(f"{camera.focus_dist_given}")
        self.aperture_edit.setText(f"{camera.lens_radius*2}")

    def get_camera(self):
        # recovery string to array from: https://stackoverflow.com/questions/57826092/how-to-convert-string-formed-by-numpy-array2string-back-to-array
        lookfrom = eval('np.array(' + self.lookfrom_edit.text() + ', np.float32)')
        lookat = eval('np.array(' + self.lookat_edit.text() + ', np.float32)')
        vup = eval('np.array(' + self.vup_edit.text() + ', np.float32)')
        img_wh = (int(self.width_edit.text()), int(self.height_edit.text()))
        fov = np.deg2rad(float(self.fov_edit.text()))
        focus_dist = eval('np.array(' + self.focus_dist_edit.text() + ', np.float32)')
        aperture = float(self.aperture_edit.text())

        if len(focus_dist.shape) == 0:
            focus_dist = focus_dist.item()

        return Camera(lookfrom, lookat, vup, img_wh, fov, focus_dist, aperture)

class CalculationThread(QThread):
    progress_signal = pyqtSignal(int, int, str)
    def __init__(self, scene, camera, samples_per_ray, max_bounces, float64_calculation, on_gpu):
        super(CalculationThread, self).__init__()
        # signal meaning:
        #  first argument: type:
        #     0: message concerning overall step, second argument meaningless
        #     1: object currently raytraced, string is the name of the object
        #     2: bounce pass, string meaningless
        #     3: color rendering, string meaningless
        #     5: everything finished, second and third arguments meaningless
        #     6: error during calculation, string is message
        self.image = None
        self.camera = camera
        self.scene = scene
        self.samples_per_ray = samples_per_ray
        self.max_bounces = max_bounces
        self.float64_calculation = float64_calculation
        self.on_gpu = on_gpu

    def __del__(self):
        self.wait()

    def run(self):
        try:
            self.image = self.camera.render(self.scene, self.samples_per_ray, max_bounces=self.max_bounces, precision=np.float64 if self.float64_calculation else np.float32, on_gpu = self.on_gpu, signal = self.progress_signal)
            self.image = (255*self.image).astype(np.uint8)
            self.progress_signal.emit(5, 0, "")
        except Exception as ex:
            self.progress_signal.emit(6,0,str(ex))
        