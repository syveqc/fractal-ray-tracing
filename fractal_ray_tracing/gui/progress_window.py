from PyQt6.QtWidgets import QProgressBar, QVBoxLayout, QWidget, QLabel

class ProgressWindow(QWidget):
    def __init__(self, signal, n_objects, n_bounces, n_primary_rays):
        super(ProgressWindow, self).__init__()
        self.signal = signal
        signal.connect(self.handle_progress)
        
        self.object_bar = QProgressBar(self)
        self.object_bar.setFormat("%v / %m")
        self.object_bar.setValue(0)
        self.object_bar.setMaximum(n_objects)

        self.bounce_bar = QProgressBar(self)
        self.bounce_bar.setFormat("%v / %m")
        self.bounce_bar.setValue(0)
        self.bounce_bar.setMaximum(n_bounces)

        self.ray_bar = QProgressBar(self)
        self.ray_bar.setValue(0)
        self.ray_bar.setMaximum(n_primary_rays)

        self.vbox = QVBoxLayout()
        self.overall_progress_label = QLabel("")
        self.vbox.addWidget(self.overall_progress_label)
        self.vbox.addWidget(QLabel("--------------------------------"))
        self.bounce_progress_label = QLabel("Bounce Progress:")
        self.vbox.addWidget(self.bounce_progress_label)
        self.vbox.addWidget(self.bounce_bar)
        self.object_progress_label = QLabel("Object Progress:")
        self.vbox.addWidget(self.object_progress_label)
        self.vbox.addWidget(self.object_bar)
        self.vbox.addWidget(QLabel("Render Progress:"))
        self.vbox.addWidget(self.ray_bar)

        self.setLayout(self.vbox)

    def handle_progress(self, progress_type, progress, msg):
        if progress_type == 0:
            self.overall_progress_label.setText(msg)
        if progress_type == 1:
            self.object_bar.setValue(progress)
            self.object_progress_label.setText(f"Object Progress: {msg}")
        if progress_type == 2:
            self.bounce_bar.setValue(progress)
            self.bounce_progress_label.setText(f"Bounce Progress: {msg}")
        if progress_type == 3:
            self.ray_bar.setValue(progress)