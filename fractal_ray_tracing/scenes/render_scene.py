from fractal_ray_tracing.raytracing import Scene, Camera
import json
from PIL import Image
import os
import numpy as np
import sys

def render_scene(filename):
    print(f"rendering {filename}")
    f = open(filename)
    json_dict = json.loads(''.join(f.readlines()).replace("\n", ""))
    scene = Scene.from_json(json_dict)
    camera = Camera.from_json(json_dict["camera"])
    max_bounces = json_dict["max_bounces"]
    samples_per_ray = json_dict["samples_per_ray"]

    image = camera.render(scene, samples_per_ray, max_bounces, on_gpu=True, precision=np.float64)
    image = (255*image).astype(np.uint8)
    Image.fromarray(image).save("../pics/" + filename[:-6] + ".png")

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("no scene given!")
        sys.exit(0)
    render_scene(sys.argv[1])