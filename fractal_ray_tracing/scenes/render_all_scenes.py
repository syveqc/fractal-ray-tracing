from fractal_ray_tracing.raytracing import Scene, Camera
from render_scene import render_scene
import json
from PIL import Image
import os
import numpy as np

for filename in os.listdir("."):
    if filename.endswith(".scene"):
        render_scene(filename)