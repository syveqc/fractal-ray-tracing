from .base_object import BaseObject
from .materials import Material
import numpy as np
import numba
from numba import cuda


def normalize(v):
    return v/(np.linalg.norm(v, axis=1, keepdims=True)+1e-10)

class IFS(BaseObject):

    def __init__(self, fs, origin, material, transformation_matrix = None, max_depth=8, normal_calculation_depth=5):
        """
        Class representing an iterated function system.

        :param fs: the contraction mappings making up the IFS.
        :param origin: the origin of the IFS, will not be its center, but rather a translation applied to the center
        :param transformation_matrix: optional transformation matrix to be applied, e.g. rotation or scaling
        :param max_depth: maximum recursion depth, determining level of detail
        :param normal_calculation_depth: over how many recursion levels the normals should be averaged, see antialiasing
                    of original paper.
        """
        super(IFS, self).__init__()
        self.fs = fs
        self.inverse_fs = np.linalg.inv(fs) # inverse of components
        self.material = material
        self.max_depth = max_depth
        self.normal_calculation_depth = normal_calculation_depth
        self.origin = origin
        self.transformation_matrix = np.diag([1.0,1.0,1.0,1.0])
        if transformation_matrix is not None:
            self.transformation_matrix[0:3,0:3] = transformation_matrix
        self.transformation_matrix[0:3,3] = origin
        self.inverse_transformation_matrix = np.linalg.inv(self.transformation_matrix)


    def _get_bounding_sphere(self):
        """
        Return approximate origin and radius of a sphere containing the fractal.

        Method
        ------
        Iteratively approximate max distances of fractals resulting from applying the contractions
        by estimating them from eigenvalues of the contractions.
        """
        origin = np.array([0,0,0,1], np.float32)
        radius = 1.0
        for _ in range(5):
            origin = np.mean([np.matmul(f, origin) for f in self.fs], axis=0)
            radius = np.max([np.linalg.norm(np.matmul(f, origin)-origin)+radius*np.abs(np.max(np.linalg.eig(f[0:3, 0:3])[0])) for f in self.fs])
        return origin[0:3], radius

    def hit_cpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using only the cpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)

        Method
        ------
        Apply transformations given by origin and transformation matrix, then allocate temporary variable ndarrays and pass
        everything to the gpu methods without explicitly invoking the gpu.
        """
        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only

        new_hits = np.zeros((ray_origins[start_index:].shape[0],), np.int)
        dtype = ray_origins.dtype
        bounding_sphere_origin, bounding_sphere_radius = self._get_bounding_sphere()

        transformed_ray_origins = np.matmul(np.concatenate((ray_origins[start_index:], np.ones((len(ray_origins[start_index:]),1))), axis=1), self.inverse_transformation_matrix.T)[:, 0:3]
        transformed_ray_directions = np.matmul(ray_directions[start_index:], self.inverse_transformation_matrix[0:3,0:3].T)

        for i in range(len(transformed_ray_origins)):
            ray_origin = transformed_ray_origins[i]
            ray_direction = transformed_ray_directions[i]
            ray_hitlist = ray_hitlists[start_index+i,:]
            original_ray_origin = np.zeros((3,), dtype)
            new_ray_direction = np.zeros((3,), dtype)
            transformed_ray_origin = np.zeros((3,), dtype)
            transformed_ray_direction = np.zeros((3,), dtype)
            tmp_point = np.zeros((3,), dtype)
            tmp_transformed_point = np.zeros((3,), dtype)
            ray_distances = np.zeros((self.max_depth, self.fs.shape[0]+1), dtype)
            transformation_history = np.zeros((self.max_depth,), np.int)
            normals = np.zeros((2,3), dtype)
            new_hit = new_hits[i:i+1]
            t = ray_sphere_intersection(ray_origin, ray_direction, bounding_sphere_origin, bounding_sphere_radius)
            if t < np.inf:
                ray_hit(ray_origin, ray_direction, ray_hitlist, self.fs, self.inverse_fs, self.fs.shape[0], bounding_sphere_origin, bounding_sphere_radius, self.max_depth, \
                    original_ray_origin, new_ray_direction, transformed_ray_origin, transformed_ray_direction, tmp_point, tmp_transformed_point, ray_distances, transformation_history, normals, self.normal_calculation_depth, new_hit, hit_only)
        if not hit_only:
            for j in np.where(new_hits == 1)[0]:
                ray_materials[j+start_index] = self.material
            ray_hitlists[start_index:,4:7] = normalize(ray_hitlists[start_index:,4:])

    def hit_gpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using the gpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)

        Method
        ------
        Apply transformations given by origin and transformation matrix, then allocate temporary ndarrays, move them
        to the gpu and pass everything to the gpu methods.
        """
        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        tpb = 32*16
        blocks = 2**30
        chunk_size = 10000000
        bounding_sphere_origin, bounding_sphere_radius = self._get_bounding_sphere()
        bounding_sphere_origin = cuda.to_device(bounding_sphere_origin)

        transformed_ray_origins = np.matmul(np.concatenate((ray_origins[start_index:], np.ones((len(ray_origins[start_index:]),1))), axis=1), self.inverse_transformation_matrix.T)[:, 0:3]
        transformed_ray_directions = np.matmul(ray_directions[start_index:], self.inverse_transformation_matrix[0:3,0:3].T)

        for i in range(0,len(transformed_ray_origins), chunk_size):
            chunk = min(len(transformed_ray_origins), i+chunk_size)-i
            ray_origins_dev = cuda.to_device(np.ascontiguousarray(transformed_ray_origins[i:i+chunk]))
            ray_directions_dev = cuda.to_device(transformed_ray_directions[i:i+chunk])
            ray_hitlists_dev = cuda.to_device(ray_hitlists[i+start_index:i+start_index+chunk])
            original_ray_origins_dev = cuda.device_array_like(transformed_ray_origins[i:i+chunk])
            new_ray_directions_dev = cuda.device_array_like(transformed_ray_directions[i:i+chunk])
            transformed_ray_origins_dev = cuda.device_array_like(ray_origins[i+start_index:i+start_index+chunk])
            transformed_ray_directions_dev = cuda.device_array_like(ray_directions[i+start_index:i+start_index+chunk])
            tmp_points = cuda.device_array_like(ray_origins[i+start_index:i+start_index+chunk])
            tmp_transformed_points = cuda.device_array_like(ray_origins[i+start_index:i+start_index+chunk])
            rays_distances = cuda.device_array((chunk, self.max_depth, self.fs.shape[0] + 1), np.float32) # last [ray, depth, len(fs)] is whether or not depth was calculated before
            transformation_histories = cuda.device_array((chunk, self.max_depth), np.int)
            normals = cuda.device_array((chunk, 2, 3), ray_origins.dtype)
            new_hits = np.zeros((chunk,), np.int)
            new_hits_dev = cuda.to_device(new_hits) 

            hit[blocks, tpb](chunk, ray_origins_dev, ray_directions_dev, ray_hitlists_dev, cuda.to_device(self.fs), cuda.to_device(self.inverse_fs), self.inverse_fs.shape[0], bounding_sphere_origin, bounding_sphere_radius, self.max_depth,
                            original_ray_origins_dev, transformed_ray_origins_dev, new_ray_directions_dev, transformed_ray_directions_dev, tmp_points, tmp_transformed_points, rays_distances, transformation_histories, normals, self.normal_calculation_depth, new_hits_dev, hit_only)

            ray_hitlists_dev.copy_to_host(ray_hitlists[i+start_index:i+start_index+chunk])
            if not hit_only:
                new_hits_dev.copy_to_host(new_hits)
                for j in np.where(new_hits == 1)[0]:
                    ray_materials[j+i+start_index] = self.material

    def get_json_dict(self):
        """
        Serialize to json.
        """
        return {"type": "ifs",
                "fs": np.array2string(self.fs,  separator=',', suppress_small=True, precision=20),
                "origin": np.array2string(self.origin,  separator=',', suppress_small=True),
                "transformation_matrix": np.array2string(self.transformation_matrix[0:3,0:3],  separator=',', suppress_small=True),
                "material": self.material.get_json_dict(),
                "max_depth": self.max_depth,
                "normal_calculation_depth": self.normal_calculation_depth
                }
        
    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct object from json.
        """
        fs = eval('np.array(' + json_dict["fs"] + ', np.float64)')
        origin = eval('np.array(' + json_dict["origin"] + ', np.float32)')
        transformation_matrix = eval('np.array(' + json_dict["transformation_matrix"] + ', np.float32)')
        material = Material.from_json(json_dict["material"])
        max_depth = json_dict["max_depth"]
        normal_calculation_depth = json_dict["normal_calculation_depth"]
        return IFS(fs, origin, material, transformation_matrix, max_depth, normal_calculation_depth)

@cuda.jit
def hit(chunk_size, ray_origins, ray_directions, ray_hitlists, fs, inverse_fs, n_fs, bounding_sphere_origin, bounding_sphere_radius, max_depth,
        new_ray_origins, transformed_ray_origins, new_ray_directions, transformed_ray_directions, tmp_points, tmp_transformed_points, rays_distances, transformation_histories, normals, normal_calculation_depth, new_hits, hit_only):
    """
    Prepare for hitting the ifs with rays on the gpu, passing further along to ray_hit.
    """
    i = cuda.threadIdx.x + cuda.blockIdx.x * cuda.blockDim.x

    if i < chunk_size:
        ray_origin = ray_origins[i,:]
        ray_direction = ray_directions[i,:]
        ray_hitlist = ray_hitlists[i,:]
        original_ray_origin = new_ray_origins[i,:]
        new_ray_direction = new_ray_directions[i,:]
        transformed_ray_origin = transformed_ray_origins[i,:]
        transformed_ray_direction = transformed_ray_directions[i,:]
        tmp_point = tmp_points[i,:]
        tmp_transformed_point = tmp_transformed_points[i,:]
        ray_distances = rays_distances[i,:,:]
        transformation_history = transformation_histories[i,:]
        new_hit = new_hits[i:i+1]
        t = ray_sphere_intersection(ray_origin, ray_direction, bounding_sphere_origin, bounding_sphere_radius)
        if t>0 and t < np.inf:
            ray_hit(ray_origin, ray_direction, ray_hitlist, fs, inverse_fs, n_fs, bounding_sphere_origin, bounding_sphere_radius, max_depth, \
                original_ray_origin, new_ray_direction, transformed_ray_origin, transformed_ray_direction, tmp_point, tmp_transformed_point, ray_distances, transformation_history, normals[i, :, :], normal_calculation_depth, new_hit, hit_only)


@numba.jit
def ray_hit(ray_origin, ray_direction, ray_hitlist, fs, inverse_fs, n_fs, sphere_origin, sphere_radius, max_depth,
        original_ray_origin, transformed_ray_origin, new_ray_direction, transformed_ray_direction, tmp_point, tmp_transformed_point, ray_distances, transformation_history, normals, normal_calculation_depth, new_hit, hit_only):
    """
    Calculate where (if) a given ray intersects the IFS. Needs a lot of temporary variables since
    allocation on the gpu is not allowed after initialization. (cuda.local.array did not work very well, was buggy)

    Method
    ------
    For each depth, transform the given sphere with f (from the contractions f) and the distance where it hit this transformed sphere.
    (note that we talk about transforming the sphere because it is easier to imagine, in reality we apply the inverse transformation
    to the ray because it is easier to calculate, since in general the sphere would deform.)
    Take the f with the smallest distance (the one the ray hit first) and continue the search by again applying the contractions, 
    calculating distances, taking the smallest distance, and so on. 
    If max_depth is reached, the transformations (that were recorded in transformation_history along the way) are applied backwards
    to get the hit point (and normals that are averaged) in the original coordinate system and the hit point is recorded.
    If at some point no intersection is recorded, we delete the distance record in the level above and reduce the depth, therefore
    taking the next closest transformation and continuing the recursion.
    If at some point at depth 0 no hit is recorded (either because none was recorded in the first place or if all were deleted)
    no rays hit and we return back.
    """
    
    depth = 0
    original_ray_origin[0] = ray_origin[0]
    original_ray_origin[1] = ray_origin[1]
    original_ray_origin[2] = ray_origin[2]
    while True:
        if ray_distances[depth, n_fs] == 0: # calculate intersections of components
            i = 0
            while i < n_fs:
                ray_distances[depth, i] = np.inf
                apply_homogenous_transformation(inverse_fs[i,:,:], ray_origin, transformed_ray_origin, False)
                apply_homogenous_transformation(inverse_fs[i,:,:], ray_direction, transformed_ray_direction, True)
                t = ray_sphere_intersection(transformed_ray_origin, transformed_ray_direction, sphere_origin, sphere_radius)
                if t > 0 and t < np.inf:
                    tmp_point[0] = transformed_ray_origin[0] + t * transformed_ray_direction[0]
                    tmp_point[1] = transformed_ray_origin[1] + t * transformed_ray_direction[1]
                    tmp_point[2] = transformed_ray_origin[2] + t * transformed_ray_direction[2]
                    apply_homogenous_transformation(fs[i,:,:], tmp_point, tmp_transformed_point, False)
                    ray_distances[depth, i] = distance_squared(ray_origin, tmp_transformed_point)
                i += 1
            ray_distances[depth, n_fs] = 1
        # search for closest intersection
        i = 0
        j = -1
        min_dist = np.inf
        while i < n_fs:
            if ray_distances[depth, i] < min_dist:
                j = i
                min_dist = ray_distances[depth, i]
            i+=1
        if j == -1: # nothing intersected
            if depth == 0: # nothing intersected at the lowest recursion depth -> no hit
                return
            # continue search in lower depth
            last_transformation_index = transformation_history[depth-1]
            ray_distances[depth-1, last_transformation_index] = np.inf
            ray_distances[depth, n_fs] = 0
            depth -= 1
            #reset ray to previous iteration
            apply_homogenous_transformation_inplace(fs[last_transformation_index,:,:], ray_origin, False)
            apply_homogenous_transformation_inplace(fs[last_transformation_index,:,:], ray_direction, True)
            continue
        
        if depth == max_depth-1: # already at max depth, color the pixel
            apply_homogenous_transformation_inplace(inverse_fs[j,:,:], ray_origin, False)
            apply_homogenous_transformation_inplace(inverse_fs[j,:,:], ray_direction, True)
            t = ray_sphere_intersection(ray_origin, ray_direction, sphere_origin, sphere_radius)
            tmp_point[0] = ray_origin[0] + t * ray_direction[0]
            tmp_point[1] = ray_origin[1] + t * ray_direction[1]
            tmp_point[2] = ray_origin[2] + t * ray_direction[2]
            normals[0,0] = tmp_point[0]-sphere_origin[0]
            normals[0,1] = tmp_point[1]-sphere_origin[1]
            normals[0,2] = tmp_point[2]-sphere_origin[2]#
            # j is not in the transformation history, so do one transformation for j, then the rest for the tranformation history
            apply_homogenous_transformation_inplace(fs[j,:,:], tmp_point, False)
            apply_homogenous_transformation_inplace(fs[j,:,:], normals[0,:], True)
            normalize_gpu(normals[0,:])
            # translate point into original coordinate system by applying transformations backwards
            # (calculating normals along the way)
            while depth > 0:
                depth -= 1
                apply_homogenous_transformation_inplace(fs[transformation_history[depth],:,:], tmp_point, False)
                apply_homogenous_transformation_inplace(fs[transformation_history[depth],:,:], normals[0,:], True)
                normalize_gpu(normals[0,:])
                if not hit_only and depth > max_depth -2 - normal_calculation_depth:
                    apply_homogenous_transformation_inplace(fs[transformation_history[depth],:,:], ray_origin, False)
                    apply_homogenous_transformation_inplace(fs[transformation_history[depth],:,:], ray_direction, True)
                    t = ray_sphere_intersection(ray_origin, ray_direction, sphere_origin, sphere_radius)
                    normals[0,0] = normals[0,0] + ray_origin[0] + t * ray_direction[0]-sphere_origin[0]
                    normals[0,1] = normals[0,1] + ray_origin[1] + t * ray_direction[1]-sphere_origin[1]
                    normals[0,2] = normals[0,2] + ray_origin[2] + t * ray_direction[2]-sphere_origin[2]

            normalize_gpu(normals[0,:])
            distance = distance_squared(original_ray_origin, tmp_point)
            if distance > 0.000001 and (ray_hitlist[0] == 0 or distance < ray_hitlist[0]): 
                new_hit[0] = 1
                ray_hitlist[0] = distance
                if not hit_only:
                    ray_hitlist[1] = tmp_point[0] # hit point
                    ray_hitlist[2] = tmp_point[1]
                    ray_hitlist[3] = tmp_point[2]
                    ray_hitlist[4] = normals[0,0] # normals
                    ray_hitlist[5] = normals[0,1]
                    ray_hitlist[6] = normals[0,2]

            return

        # continue search at the component with lowest distance
        transformation_history[depth] = j
        apply_homogenous_transformation_inplace(inverse_fs[j,:,:], ray_origin, False)
        apply_homogenous_transformation_inplace(inverse_fs[j,:,:], ray_direction, True)
        depth +=1
        


@numba.jit
def distance_squared(x, y):
    """
    Return the squared distance of x and y, since sqrt is not very calculation efficient 
    and not needed for comparison.
    """
    return (x[0]-y[0])**2+(x[1]-y[1])**2+(x[2]-y[2])**2

@numba.jit
def ray_sphere_intersection(ray_origin, ray_direction, sphere_origin, sphere_radius):
    """
    Calculate the intersection of a ray with the given sphere and return the distance in terms
    of the ray_direction.
    """
    co0 = ray_origin[0]-sphere_origin[0]
    co1 = ray_origin[1]-sphere_origin[1]
    co2 = ray_origin[2]-sphere_origin[2]
    a = ray_direction[0]**2+ray_direction[1]**2+ray_direction[2]**2
    half_b = co0*ray_direction[0]+co1*ray_direction[1]+co2*ray_direction[2]
    c = co0**2+co1**2+co2**2-sphere_radius**2
    disc = half_b**2-a*c
    if disc>0:
        t = (-half_b-disc**0.5) / a
        if t > 0:
            return t
        t = (-half_b+disc**0.5) / a
        if t > 0:
            return t
    return np.inf

@numba.jit
def normalize_gpu(v):
    """
    Normalizes v in a numba.cuda friendly form.
    """
    norm = (v[0]**2+v[1]**2+v[2]**2)**0.5
    v[0] = v[0] / norm
    v[1] = v[1] / norm
    v[2] = v[2] / norm

@numba.jit
def apply_homogenous_transformation_inplace(A, x, linear_part_only):
    """
    Apply homogenous transformation ``A`` (4x4 matrix representing a 3d affine transformation) to a 3d vector ``x``,
    writing the result into ``x``.

    Method
    ------
    Hardcoded multiplication as this should be fastest because of the fixed dimensions.
    """
    result_0 = A[0,0]*x[0] + A[0,1]*x[1] + A[0,2]*x[2]
    result_1 = A[1,0]*x[0] + A[1,1]*x[1] + A[1,2]*x[2]
    result_2 = A[2,0]*x[0] + A[2,1]*x[1] + A[2,2]*x[2]
    if not linear_part_only:
        result_0 += A[0,3]
        result_1 += A[1,3]
        result_2 += A[2,3]
    x[0] = result_0
    x[1] = result_1
    x[2] = result_2

@numba.jit
def apply_homogenous_transformation(A, x, result, linear_part_only):
    """
    Apply homogenous transformation ``A`` (4x4 matrix representing a 3d affine transformation) to a 3d vector ``x``
    and return the resulting 3d vector in ``result``.

    Method
    ------
    Hardcoded multiplication as this should be fastest because of the fixed dimensions.
    """

    result[0] = A[0,0]*x[0] + A[0,1]*x[1] + A[0,2]*x[2]
    result[1] = A[1,0]*x[0] + A[1,1]*x[1] + A[1,2]*x[2]
    result[2] = A[2,0]*x[0] + A[2,1]*x[1] + A[2,2]*x[2]
    if not linear_part_only:
        result[0] += A[0,3]
        result[1] += A[1,3]
        result[2] += A[2,3]