import numpy as np

class Material:
    def __init__(self):
        """
        Abstract class representing a material, which decides how rays reflect and which colors they take on.
        """
        self.k_ambient = 0
        self.k_diffus = 0
        self.k_spectacular = 0
        self.n = 1
        pass

    def get_color(self, 
                    color:np.ndarray, 
                    r:np.ndarray, 
                    v:np.ndarray, 
                    normal:np.ndarray, 
                    light_vector:np.ndarray, 
                    I_in:float):
        """
        Calculate the ray color, depending on given secondary ray color.

        :param color: color of the secondary ray
        :param r: direction of the outgoing light ray, normalized
        :param v: direction the camera is looking, normalized
        :param normal: normal vector of the surface, normalized
        :param light_vector: direction the light is going in, normalized
        :param I_in: I_in parameter of the phong model
        """
        raise NotImplementedError()

    def get_secondary_rays(self, origin, direction, intersection, normal):
        """
        Return a secondary ray, if none is emitted, (None, None) is returned.
        """
        raise NotImplementedError()

    def get_json_dict(self):
        """
        Serialize to json.
        """
        raise NotImplementedError()

    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct object from json
        """
        if json_dict["type"] == "solid":
            json_dict["color"] = eval('np.array(' + json_dict["color"] + ', np.float32)')
            return MaterialSolid(**json_dict)
        return None


class MaterialSolid:
    def __init__(self, 
                color = np.array([1.0, 1.0, 1.0]), 
                reflection_weight = 0.0, 
                k_ambient = 0.3, 
                k_diffus = 0.6, 
                k_spectacular=0.0, 
                n = 5, 
                **kwargs):
        """
        Solid material, with reflection.

        :param color: color of the object
        :param reflection_weight: between 0 and 1, how much of the color of the objects comes from the secondary ray color.
        :param k_ambient: k_ambient parameter from the phong model
        :param k_diffus: k_diffus parameter from the phong model
        :param k_spectacular: k_spectacular parameter from the phong model
        :param n: n parameter from the phong model
        :param kwargs: none supported, just here so any arguments can be piped into constructor
        """
        super(MaterialSolid, self).__init__()
        self.k_ambient = k_ambient
        self.k_diffus = k_diffus
        self.k_spectacular = k_spectacular
        self.n = n
        self.color = color
        self.reflection_weight = reflection_weight

    def get_color(self, 
                    color:np.ndarray, 
                    r:np.ndarray, 
                    v:np.ndarray, 
                    normal:np.ndarray, 
                    light_vector:np.ndarray, 
                    I_in:float):
        """
        Calculate the ray color, depending on given secondary ray color.

        :param color: color of the secondary ray
        :param r: direction of the outgoing light ray, normalized
        :param v: direction the camera is looking, normalized
        :param normal: normal vector of the surface, normalized
        :param light_vector: direction the light is going in, normalized
        :param I_in: I_in parameter of the phong model
        """
        return (1-self.reflection_weight)*apply_phong(self.color, r, v, normal, light_vector, I_in, self) + self.reflection_weight*color
    
    def get_secondary_rays(self, origin, direction, intersection, normal, reflected_direction):
        """
        Return a secondary ray, if reflection_weight is 0, none is returned, else secondary ray
        is calculated by spectacular reflection.
        """
        if self.reflection_weight == 0.0:
            return None, None
        return intersection, reflected_direction

    def get_json_dict(self):
        return {"type": "solid",
                "color": np.array2string(self.color,  separator=',', suppress_small=True),
                "k_ambient": self.k_ambient,
                "k_diffus": self.k_diffus,
                "k_spectacular": self.k_spectacular,
                "n": self.n,
                "reflection_weight": self.reflection_weight}

def apply_phong(color, r, v, normal, light_vector, I_in, material):
    """
    Apply the phong model with the given parameters.
    """
    I_ambient = material.k_ambient # for simplicity, I_a is 1

    I_diffus = I_in * material.k_diffus * max(0,np.dot(light_vector, normal))

    I_spectacular = I_in * material.k_spectacular * (material.n+2)/(2*np.pi)*max(0,np.dot(r,v))**material.n

    return color * (I_ambient + I_diffus + I_spectacular)
