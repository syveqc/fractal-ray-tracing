from .light_source import LightSource
from .materials import Material

import numpy as np
import numba

# helper functions
def normalize(v):
    return v/(np.linalg.norm(v, axis=1, keepdims=True)+1e-10)
    
def norm_squared(x):
    return x[0]**2+x[1]**2+x[2]**2

# main class
class RayCollection:
    def __init__(self, 
                origins:np.ndarray, 
                directions:np.ndarray, 
                precision=np.float32, 
                hits_only = False):
        """
        Class describing rays.

        :param origins: a (N,3) vector representing the origins of the rays, e.g. the camera
        :param directions: a (N,3) vector representing the directions the rays point
        :param precision: precision to use for calculations, on gpu only np.float32 and np.float64 are available
        :param hits_only: whether or not only hit distances should be recorded or hit points and normals as well, 
                        used for shadow rays vs normal rays.

        """
        self.N = len(origins)
        self.hits_only = hits_only
        self.precision = precision
        if origins.dtype != precision:
            self.origins = origins.astype(precision)
        else:
            self.origins = origins
        if directions.dtype != precision:
            self.directions = directions.astype(precision)
        else:
            self.directions = directions
        
        # hitlist, containing: (if hits_only, only one dimensional)
        #  0: distance square to origin if something was hit, 0 otherwise
        #  1-3: coordinate of ray bounce
        #  4-6: surface normal
        self.hitlists = np.zeros((len(origins), 1 if hits_only else 7))
        if not hits_only:
            self.pixel_ray_mapping = [[i] for i in range(self.N)] # which rays contribute to the given pixel
            self.materials = [None for _ in range(self.N)]
            self.worked_rays_index = 0
        
    def get_colors(self, 
                    light_source:LightSource, 
                    shadow_rays, 
                    shadow_ray_dict:dict, 
                    signal=None, 
                    background_color:np.ndarray=np.array([31./255., 104./255., 204./255.]), 
                    light_source_num=0):
        """
        Return an image from already hit rays.

        :param light_source: the light source to use
        :param shadow rays: RayCollection of the shadow rays
        :param shadow_ray_dict: which index of which ray corresponds to which shadow ray
        :param signal: Qt signal to emit status updates, if None no updates are sent
        :param background_color: the color to use for rays that hit nothing
        :param light_source_num: current light source number, used for reporting with the signal, 
                        useless otherwise 

        Method
        ------
        Phong parameters are calculated first, making use of numpy arithmetics, speeding up the later process
        considerably. Then the rays of each pixel are worked through backwards, thus calculating later emmitted
        rays first. (So e.g. if a ray hits a plane, then a sphere, then nothing, first the last ray gets colored
        in the background color, then the next ray gets a little bit of the sphere color mixed in, the next one mixes
        a bit of the plane color in, then the pixel gets colored in that color)
        If a shadow ray of this pixel hit, the color is multiplied by 0.8, therefore darkening it.
        """
        colors = np.ones((self.N, 3))
        points = self.hitlists[:,1:4]
        normals = self.hitlists[:, 4:]
        light_vectors = normalize(light_source.origin - points)
        # calculate reflected light vector, see https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
        rs = normalize(light_vectors - 2*np.sum(light_vectors*normals, axis=1, keepdims=True)*normals)
        vs = normalize(points-self.origins)
        for j, ray_indices in enumerate(self.pixel_ray_mapping):
            color = background_color
            for i in reversed(ray_indices):
                if self.hitlists[i,0]:
                    color = self.materials[i].get_color(color, rs[i], vs[i], normals[i], light_vectors[i], light_source.I_in)
                    shadow_i = shadow_ray_dict[i]
                    if shadow_rays.hitlists[shadow_i, 0] and shadow_rays.hitlists[shadow_i, 0] < norm_squared(shadow_rays.directions[shadow_i]):
                        color *= 0.8
            colors[i, :] = color
            if signal is not None:
                signal.emit(3, light_source_num*self.N+j, "")
        return colors

    def produce_secondary_rays(self):
        """
        Produce secondary rays from materials.

        Method
        ------
        Pre-calculating the reflected vectors using numpy arithmetics speeds up the process considerably.
        Otherwise, a secondary ray is generated when the original ray hit something, then the
        new rays are added to the RayCollection and hitlists, materials are extended.
        Also the ray origins are moved a little bit in the direction of the rays to prevent them from hitting 
        the same object again by calculation errors. 
        """
        start_index = len(self.origins)
        reflected_vectors = self.directions[self.worked_rays_index:] - 2*np.sum(self.directions[self.worked_rays_index:]*self.hitlists[self.worked_rays_index:, 4:], axis=1, keepdims=True)*self.hitlists[self.worked_rays_index:, 4:]
        new_origins = np.zeros((self.N, 3), self.origins.dtype)
        new_directions = np.zeros((self.N, 3), self.origins.dtype)
        ray_index = 0
        for ray_indices in self.pixel_ray_mapping:
            i = ray_indices[-1]
            if i < self.worked_rays_index or self.hitlists[i,0] == 0:
                continue
            origin, direction = self.materials[i].get_secondary_rays(self.origins[i], self.directions[i], self.hitlists[i, 1:4], self.hitlists[i, 4:], reflected_vectors[i-self.worked_rays_index])
            if origin is not None:
                new_origins[ray_index,:] = origin
                new_directions[ray_index,:] = direction
                ray_indices.append(ray_index+start_index)
                ray_index += 1

        old_worked_index = self.worked_rays_index
        self.worked_rays_index = len(self.origins)

        self.origins = np.concatenate((self.origins, new_origins[:ray_index]), axis=0)
        self.directions = np.concatenate((self.directions, new_directions[:ray_index]), axis=0)
        self.origins[old_worked_index:] = self.origins[old_worked_index:] + 0.00001*self.directions[old_worked_index:]
        self.hitlists = np.concatenate((self.hitlists, np.zeros((ray_index, 7))), axis=0)
        self.materials.extend([None]*ray_index)

    def get_shadow_ray_collection(self, light_source:LightSource):
        """
        Shadow rays are calculated as rays from hit points to the given light source.
        They then are moved a little bit in the direction of the rays to prevent them 
        from hitting the same object again by calculation errors
        """
        shadow_origins = self.hitlists[:, 1:4]
        shadow_directions = light_source.origin - self.hitlists[:, 1:4]
        new_directions = light_source.origin - self.hitlists[:, 1:4]

        I = np.where(self.hitlists[:,0]>0)[0]

        shadow_rays = RayCollection(shadow_origins[I,:]+0.01*shadow_directions[I,:], shadow_directions[I,:], self.precision, hits_only=True)

        return shadow_rays, {index: item for item, index in enumerate(I)}
