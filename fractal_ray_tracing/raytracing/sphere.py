from .base_object import BaseObject
from .materials import Material
import numpy as np
import numba
from numba import cuda

# helper function
def normalize(v):
    return v/(np.linalg.norm(v, axis=1, keepdims=True)+1e-10)

# main class
class Sphere(BaseObject):
    def __init__(self, origin, radius, material:Material):
        """
        Construct a 3d sphere by origin an radius.

        :param origin: 3d numpy array representing the origin of the sphere
        :param radius: radius of the sphere
        :param material: the material of the sphere, determining how it is rendered
        """
        super(Sphere, self).__init__()
        self.origin = origin
        self.radius = radius
        self.material = material


    def hit_cpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using only the cpu.
        """
        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        new_hits = np.zeros((ray_origins[start_index:].shape[0],), np.int)
        for i in range(len(ray_origins[start_index:])):
            hit_ray(ray_origins[start_index:], ray_directions[start_index:], ray_hitlists[start_index:], self.origin, self.radius, new_hits, i, hit_only)

        if not hit_only:
            for j in np.where(new_hits == 1)[0]:
                ray_materials[j+start_index] = self.material
            ray_hitlists[start_index:,4:7] = normalize(ray_hitlists[start_index:,4:])

    def hit_gpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using the gpu.
        """
        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        tpb = 32*32
        blocks = 2**20
        ray_origins_dev = cuda.to_device(ray_origins[start_index:],)
        ray_directions_dev = cuda.to_device(ray_directions[start_index:])
        ray_hitlists_dev = cuda.to_device(ray_hitlists[start_index:])
        new_hits = np.zeros((ray_origins[start_index:].shape[0],), np.int)
        new_hits_dev = cuda.to_device(new_hits) 
        hit[blocks, tpb](ray_origins_dev, ray_directions_dev, ray_hitlists_dev, cuda.to_device(self.origin), self.radius, new_hits_dev, hit_only)

        ray_hitlists_dev.copy_to_host(ray_hitlists[start_index:])
        if not hit_only:
            new_hits_dev.copy_to_host(new_hits)
            for j in np.where(new_hits == 1)[0]:
                ray_materials[j+start_index] = self.material
            ray_hitlists[start_index:,4:7] = normalize(ray_hitlists[start_index:,4:])

    def get_json_dict(self):
        return {"type": "sphere",
                "origin": np.array2string(self.origin,  separator=',', suppress_small=True),
                "radius": self.radius,
                "material": self.material.get_json_dict()}

    
    @staticmethod
    def from_json(json_dict):
        origin = eval('np.array(' + json_dict["origin"] + ', np.float32)')
        radius = json_dict["radius"]
        material = Material.from_json(json_dict["material"])
        return Sphere(origin, radius, material)


@cuda.jit
def hit(ray_origins, ray_directions, ray_hitlists, origin, radius, new_hits, hit_only):
    """
    Prepare for hitting the sphere with rays on the gpu, passing further along to ray_hit.
    """
    start_i = cuda.grid(1)
    grid_i = cuda.gridDim.x * cuda.blockDim.x
    for i in range(start_i, len(ray_origins), grid_i):
        # intersection with a sphere, code taken from rt with cuda example
        hit_ray(ray_origins, ray_directions, ray_hitlists, origin, radius, new_hits, i, hit_only)

@numba.jit
def hit_ray(ray_origins, ray_directions, ray_hitlists, origin, radius, new_hits, i, hit_only):
    """
    Calculate where (if) a given ray intersects the sphere.

    Method
    ------
    Standard quadratic equation solving.
    """
    co0 = ray_origins[i, 0]-origin[0]
    co1 = ray_origins[i, 1]-origin[1]
    co2 = ray_origins[i, 2]-origin[2]
    a = ray_directions[i, 0]**2+ray_directions[i, 1]**2+ray_directions[i, 2]**2
    half_b = co0*ray_directions[i, 0]+co1*ray_directions[i, 1]+co2*ray_directions[i, 2]
    c = co0**2+co1**2+co2**2-radius**2
    disc = half_b**2-a*c
    if disc>0:
        t = (-half_b-disc**0.5) / a
        if t <= 0:
            t = (-half_b+disc**0.5) / a
        if t <= 0:
            return

        hit_0 = ray_origins[i,0] + t*ray_directions[i,0]
        hit_1 = ray_origins[i,1] + t*ray_directions[i,1]
        hit_2 = ray_origins[i,2] + t*ray_directions[i,2]
        distance = (ray_origins[i, 0]-hit_0)**2+(ray_origins[i, 1]-hit_1)**2+(ray_origins[i, 2]-hit_2)**2
        if distance > 0.000001 and (ray_hitlists[i, 0] == 0 or distance < ray_hitlists[i, 0]):
            new_hits[i] = 1
            ray_hitlists[i, 0] = distance
            if not hit_only:
                ray_hitlists[i, 1] = hit_0
                ray_hitlists[i, 2] = hit_1
                ray_hitlists[i, 3] = hit_2
                ray_hitlists[i, 4] = (ray_hitlists[i,1] - origin[0])
                ray_hitlists[i, 5] = (ray_hitlists[i,2] - origin[1])
                ray_hitlists[i, 6] = (ray_hitlists[i,3] - origin[2])