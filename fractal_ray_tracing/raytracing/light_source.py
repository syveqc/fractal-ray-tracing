import numpy as np

class LightSource:
    def __init__(self, origin, I_in):
        """
        Class representing a (point) light source.

        :param origin: the origin of the light source
        :param I_in: the light intensity parameter I_in from the phong model.
        """ 
        self.origin = origin
        self.I_in = I_in

    def get_json_dict(self):
        """
        Serialize to json.
        """
        return {"origin": np.array2string(self.origin,  separator=',', suppress_small=True),
                "I_in": self.I_in}

    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct object from json.
        """
        return LightSource(eval('np.array(' + json_dict["origin"] + ', np.float32)'), json_dict["I_in"])