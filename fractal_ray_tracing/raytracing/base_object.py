import numpy as np
from .ray_collection import RayCollection
from .materials import Material

from numba import cuda

class BaseObject:
    def __init__(self):
        """
        Base class for renderable objects.
        """
        self.material = None

    def hit_cpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using only the cpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)
        """
        raise NotImplementedError("hit on the cpu is not implemented for this object!")

    def hit_gpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the sphere using the gpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)
        """
        raise NotImplementedError("hit on the gpu is not implemented for this object!")
    
    def hit_rays(self, rays: RayCollection, on_gpu=True):
        """
        Parses and passes the ``rays`` to the approprate hit method on CPU or GPU depending on ``on_gpu``.
        """
        hit_only = rays.hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        if not hit_only and len(rays.origins) == rays.worked_rays_index:
            return
        if on_gpu:
            self.hit_gpu(rays.origins, rays.directions, rays.hitlists, [] if hit_only else rays.materials, 0 if hit_only else rays.worked_rays_index)
        else:
            self.hit_cpu(rays.origins, rays.directions, rays.hitlists, [] if hit_only else rays.materials, 0 if hit_only else rays.worked_rays_index)

    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct object from json.
        """
        # import here to circumvent circular import
        from .sphere import Sphere
        from .ifs import IFS
        from .plane import Plane

        if json_dict["type"] == "sphere":
            obj = Sphere.from_json(json_dict)
            return obj
        if json_dict["type"] == "ifs":
            obj = IFS.from_json(json_dict)
            return obj
        if json_dict["type"] == "plane":
            obj = Plane.from_json(json_dict)
            return obj

        # given type not supported
        return None