# file taken from python-ray-tracing-with-cuda-example and adapted

import numpy as np
from .ray_collection import RayCollection
from .scene import Scene
from typing import List
import gc

def normalize(v):
    return v/(np.linalg.norm(v)+1e-10)


def random_in_unit_disk(N):
    phi = 2*np.pi*np.random.random(N)
    r = np.random.random(N) ** 0.5
    x = r * np.sin(phi)
    y = r * np.cos(phi)
    z = np.zeros_like(x)
    return np.stack((x, y, z), 1).astype(np.float32)


class Camera:
    def __init__(self, 
                 lookfrom=np.float32([0, 0, 0]), 
                 lookat=np.float32([0, 0, -1]),
                 vup=np.float32([0, 0, 1]),
                 img_wh=(480, 270),
                 fov=np.pi/2,
                 focus=0.,
                 aperture=0.,
                 ):
        """
        Class representing a camera.

        :param lookfrom: the origin of the camera, i.e. where it is positioned.
        :param lookat: 3d point the camera looks at
        :param vup: vector determining where up is for the camera
        :param img_wh: tuple of (width, height) of the output image
        :param fov: field of view in radians
        :param focus: either a number determining the focus distance, if -1 lookat will be in focus, or
                    a 3d point that should be in focus
        :param aperture: the aperture of the camera, determining depth of field
        """

        self.origin = lookfrom
        self.lookat = lookat
        self.vup = vup
        self.fov = fov
        self.focus_dist_given = focus

        self.w = normalize(lookfrom-lookat)
        self.u = normalize(np.cross(vup, self.w))
        self.v = np.cross(self.w, self.u)

        if type(focus) == np.ndarray:
            self.focus_dist = np.linalg.norm(lookfrom-focus)
        elif focus <= 0:
            self.focus_dist = np.linalg.norm(lookfrom-lookat)
        else:
            self.focus_dist = focus
        self.lens_radius = aperture/2

        self.W = img_wh[0]
        self.H = img_wh[1]
        self.focal = 0.5*self.W/np.tan(0.5*fov)

        self.i, self.j = np.mgrid[:self.H,:self.W][::-1]

    def get_json_dict(self):
        """
        Serialize to json.
        """
        json_dict = {"width": self.W,
                    "height": self.H,
                    "lookfrom": np.array2string(self.origin,  separator=',', suppress_small=True),
                    "lookat": np.array2string(self.lookat,  separator=',', suppress_small=True),
                    "vup": np.array2string(self.vup,  separator=',', suppress_small=True),
                    "fov": int(np.degrees(self.fov)),
                    "focus_dist": np.array2string(self.focus_dist_given, separator=',', suppress_small=True) if type(self.focus_dist_given) == np.ndarray else self.focus_dist_given,
                    "aperture": self.lens_radius*2}

        return json_dict
    
    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct camera object from json.
        """
        width = json_dict["width"]
        height= json_dict["height"]
        lookfrom = eval('np.array(' + json_dict["lookfrom"] + ', np.float32)')
        lookat = eval('np.array(' + json_dict["lookat"] + ', np.float32)')
        vup = eval('np.array(' + json_dict["vup"] + ', np.float32)')
        fov = np.deg2rad(json_dict["fov"])
        focus_dist = eval('np.array(' + str(json_dict["focus_dist"]) + ', np.float32)')
        aperture = json_dict["aperture"]

        if len(focus_dist.shape) == 0 or focus_dist.shape[0] == 1:
            focus_dist = focus_dist.item()

        return Camera(lookfrom, lookat, vup, (width, height), fov, focus_dist, aperture)


    def get_rays(self, samples_per_ray=1, precision=np.float32) -> RayCollection:
        """
        Generate rays of the current camera and return as RayCollection.
        """
        i = np.stack([self.i]*samples_per_ray, -1)
        j = np.stack([self.j]*samples_per_ray, -1)

        if samples_per_ray > 1:
            i = i + np.random.random(i.shape)
            j = j + np.random.random(j.shape)

        rays_d = (i[..., np.newaxis]-self.W/2)/self.focal*self.focus_dist*self.u + \
                 -(j[..., np.newaxis]-self.H/2)/self.focal*self.focus_dist*self.v + \
                 -self.focus_dist*self.w
        
        rays_d = rays_d.reshape(-1, 3).astype(np.float32)
        rays_o = np.tile(self.origin, (len(rays_d), 1))

        # compute ray offset for depth of field
        if self.lens_radius > 0:
            offset = self.lens_radius * random_in_unit_disk(len(rays_d)) # (N, 3)
            offset = offset[:, 0:1] * self.u + offset[:, 1:2] * self.v
            rays_o += offset
            rays_d -= offset

        return RayCollection(rays_o, rays_d, precision)

    def render(self, scene:Scene, samples_per_ray=1, max_bounces = 5, on_gpu = True, precision=np.float32, signal = None):
        """
        Renders the given ``scene`` from the camera viewport

        :param scene: the scene to render
        :param samples_per_ray: how many rays are used per pixel, useful for depth of field effects or (not supported yet) stochastic reflections in materials.
        :param max_bounces: how often any ray may bounce on any surface
        :param on_gpu: whether or not force calculation on the gpu, if no gpu is available this will crash the program
        :param precision: which precision to use for calculation, on gpu only np.float32 and np.float64 are available
        :param signal: Qt signal to emit status updates, if None no updates are sent

        Method
        -------
        Generate rays, then alternate between hitting objects and producing secondary rays from bounces on hit objects.
        After no more rays are generated or max number of bounces is reached, a shadow ray is generated for each
        ray that hit an object (i.e. a ray from the hit point to the origin of a light source). Then the image is calculated
        for each light source by calling RayCollection.get_colors() and the final image is produced by averaging over all
        images created from the light sources in the scene.
        """
        if signal is not None:
            signal.emit(0, 0, "Generating rays...")
        rays = self.get_rays(samples_per_ray, precision)
        
        if signal is not None:
            signal.emit(0, 0, "Finished generating rays, starting bounces...")

        # bounce rays around
        for i in range(max_bounces):
            if signal is not None:
                signal.emit(2, i, f"{len(rays.origins)-rays.worked_rays_index} rays produced")
            for j, obj in enumerate(scene.objects):
                if signal is not None:
                    signal.emit(1, j, f"{type(obj).__name__}")
                obj.hit_rays(rays, on_gpu)
            if signal is not None:
                signal.emit(1, len(scene.objects), "")
            if i < max_bounces-1:
                if signal is not None:
                    signal.emit(0, 0, "Generating secondary rays...")
                rays.produce_secondary_rays()
                if signal is not None:
                    signal.emit(0, 0, "Secondary rays generated, bouncing...")
            gc.collect() # free memory, can be important for larger images
        
        if signal is not None:
            signal.emit(2, max_bounces, "")


        if signal is not None:
            signal.emit(0, 0, "Finished bounces, starting render...")

        # shadow rays
        colors = np.zeros((self.H*self.W, 3))
        for i, light_source in enumerate(scene.light_sources):
            if signal is not None:
                signal.emit(0, 0, "Handling shadow rays...")
            shadow_rays, shadow_ray_dict = rays.get_shadow_ray_collection(light_source)
            for j, obj in enumerate(scene.objects):
                if signal is not None:
                    signal.emit(1, j, f"{type(obj).__name__}")
                obj.hit_rays(shadow_rays, on_gpu)
            gc.collect()
            if signal is not None:
                signal.emit(1, len(scene.objects), "")

            if signal is not None:
                signal.emit(0, 0, f"Rendering light source number {i+1}...")
            
            ray_colors = rays.get_colors(light_source, shadow_rays, shadow_ray_dict, signal, scene.background_color, i)
            all_rays = ray_colors.reshape(-1, samples_per_ray, 3)
            colors += all_rays.mean(axis=1)
        
        colors /= len(scene.light_sources)

        if signal is not None:
            signal.emit(0, 0, "All finished!")
        return colors.reshape(self.H, self.W, 3)