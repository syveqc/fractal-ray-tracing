from .base_object import BaseObject
from .materials import Material
import numpy as np
import numba
from numba import cuda

# helper
def normalize(v):
    return v/(np.linalg.norm(v, axis=1, keepdims=True)+1e-10)

# main class
class Plane(BaseObject):
    def __init__(self, origin, normal_vector, material):
        """
        Construct a 3d plane by origin and normal_vector.
        """
        super(Plane, self).__init__()
        self.origin = origin
        self.normal_vector = normal_vector/np.linalg.norm(normal_vector)
        self.material = material


    def hit_cpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the plane using only the cpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)
        """

        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        new_hits = np.zeros((ray_origins[start_index:].shape[0],), np.int)
        for i in range(len(ray_origins[start_index:])):
            hit_ray(ray_origins[start_index:], ray_directions[start_index:], ray_hitlists[start_index:], self.origin, self.normal_vector, new_hits, i, hit_only)
        
        if not hit_only:
            for j in np.where(new_hits == 1)[0]:
                ray_materials[j+start_index] = self.material

    def hit_gpu(self, ray_origins, ray_directions, ray_hitlists, ray_materials, start_index):
        """
        Calculate hits of rays with the plane using the gpu.

        :param ray_origins: Nx3 ndarray of origins of the rays to hit the object with.
        :param ray_directions: Nx3 ndarray of directions of the rays to hit the object with.
        :param ray_hitlists: NxM, M=1 or M=7, list to save the hits of the rays, see also RayCollection.ray_hitlists.
        :param ray_materials: List with N entries representing the material of the object the ray has previously hit.
        :param start_index: where to start in the rays, used to ignore the first entries of the rays. 
                            (technical reason: handing a function a slice of a list turns it into a "call-by-value",
                             call instead of a call-by-reference one, making a sliced call infeasible for the ray_materials list)
        """
        hit_only = ray_hitlists.shape[1] == 1 # if hitlist is only one entry, calculate hits only
        tpb = 32*32
        blocks = 2**20
        ray_origins_dev = cuda.to_device(ray_origins[start_index:],)
        ray_directions_dev = cuda.to_device(ray_directions[start_index:])
        ray_hitlists_dev = cuda.to_device(ray_hitlists[start_index:])
        new_hits = np.zeros((ray_origins[start_index:].shape[0],), np.int)
        new_hits_dev = cuda.to_device(new_hits) 
        hit[blocks, tpb](ray_origins_dev, ray_directions_dev, ray_hitlists_dev, cuda.to_device(self.origin), cuda.to_device(self.normal_vector), new_hits_dev, hit_only)

        ray_hitlists_dev.copy_to_host(ray_hitlists[start_index:])
        if not hit_only:
            new_hits_dev.copy_to_host(new_hits)
            for j in np.where(new_hits == 1)[0]:
                ray_materials[j+start_index] = self.material

    def get_json_dict(self):
        """
        Serialize to json.
        """
        return {"type": "plane",
                "origin": np.array2string(self.origin,  separator=',', suppress_small=True),
                "normal_vector": np.array2string(self.normal_vector,  separator=',', suppress_small=True),
                "material": self.material.get_json_dict()}

    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct the object from json.
        """
        origin = eval('np.array(' + json_dict["origin"] + ', np.float32)')
        normal_vector = eval('np.array(' + json_dict["normal_vector"] + ', np.float32)')
        material = Material.from_json(json_dict["material"])
        return Plane(origin, normal_vector, material)


@cuda.jit
def hit(ray_origins, ray_directions, ray_hitlists, origin, normal_vector, new_hits, hit_only):
    """
    Prepare for hitting the plane with rays on the gpu, passing further along to ray_hit.
    """
    start_i = cuda.grid(1)
    grid_i = cuda.gridDim.x * cuda.blockDim.x
    for i in range(start_i, len(ray_origins), grid_i):
        hit_ray(ray_origins, ray_directions, ray_hitlists, origin, normal_vector, new_hits, i, hit_only)

@numba.jit
def hit_ray(ray_origins, ray_directions, ray_hitlists, origin, normal_vector, new_hits, i, hit_only):
    """
    Calculate where (if) a given ray intersects the plane.

    Method
    ------
    Standard linear equation solving.
    """

    dot = normal_vector[0]*ray_directions[i, 0]+normal_vector[1]*ray_directions[i, 1]+normal_vector[2]*ray_directions[i, 2]
    if abs(dot) > 0.001:
        w_0 = ray_origins[i, 0]-origin[0]
        w_1 = ray_origins[i, 1]-origin[1]
        w_2 = ray_origins[i, 2]-origin[2]

        t = -normal_vector[0]*w_0-normal_vector[1]*w_1-normal_vector[2]*w_2
        t = t / dot
        if t>0:
            hit_0 = ray_origins[i,0] + t*ray_directions[i,0]
            hit_1 = ray_origins[i,1] + t*ray_directions[i,1]
            hit_2 = ray_origins[i,2] + t*ray_directions[i,2]
            distance = (ray_origins[i, 0]-hit_0)**2+(ray_origins[i, 1]-hit_1)**2+(ray_origins[i, 2]-hit_2)**2
            if distance > 0.000001 and (ray_hitlists[i, 0] == 0 or distance < ray_hitlists[i, 0]):
                new_hits[i] = 1
                ray_hitlists[i, 0] = distance
                if not hit_only:
                    ray_hitlists[i, 1] = hit_0
                    ray_hitlists[i, 2] = hit_1
                    ray_hitlists[i, 3] = hit_2
                    ray_hitlists[i, 4] = normal_vector[0]
                    ray_hitlists[i, 5] = normal_vector[1]
                    ray_hitlists[i, 6] = normal_vector[2]