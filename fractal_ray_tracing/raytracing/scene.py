import numpy as np
from .base_object import BaseObject
from .light_source import LightSource
from typing import List
import json

class Scene:
    def __init__(self, background_color = np.array([31./255., 104./255., 204./255.])):
        """
        Scene, consisting of objects and light sources.

        :param background_color: color of rays that do not hit anything
        """
        self.objects: List[BaseObject] = []
        self.light_sources: List[LightSource] = []
        self.background_color = background_color

    def add_object(self, obj:BaseObject):
        """
        Add raytraced object.
        """
        self.objects.append(obj)

    def add_light_source(self, light_source:LightSource):
        """
        Add a light source
        """
        self.light_sources.append(light_source)

    def get_json_string(self, camera = None, max_bounces = None, samples_per_ray=None):
        """
        Serialize to json, returned as a string.
        """
        json_dict = {}
        json_dict["objects"] = [obj.get_json_dict() for obj in self.objects]
        json_dict["light_sources"] = [light_source.get_json_dict() for light_source in self.light_sources]
        if camera is not None:
            json_dict["camera"] = camera.get_json_dict()
        
        if max_bounces:
            json_dict["max_bounces"] = max_bounces
        if samples_per_ray:
            json_dict["samples_per_ray"] = samples_per_ray

        return json.dumps(json_dict, indent=4)

    @staticmethod
    def from_json(json_dict):
        """
        Reconstruct the object from json.
        """
        scene = Scene()
        if "background_color" in json_dict:
            scene.background_color = eval('np.array(' + json_dict["background_color"] + ', np.float32)')
        for obj_string in json_dict["objects"]:
            scene.add_object(BaseObject.from_json(obj_string))
        for light_source_string in json_dict["light_sources"]:
            scene.add_light_source(LightSource.from_json(light_source_string))
        return scene